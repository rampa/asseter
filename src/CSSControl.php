<?php
    /**
     * User: Jan Červený
     * Date: 16.1.2015
     * Time: 10:04
     */

    namespace Rampus\Asseter;

    class CSSControl extends BaseAsseter {
        public function getElement($out) {
            return "<link type=\"text/css\" rel=\"stylesheet\" href=\"$out\" />" . PHP_EOL;
        }
    }