<?php
/**
 * User: Jan Červený
 * Date: 16.1.2015
 * Time: 9:39
 */

namespace Rampus\Asseter;


trait AsseterTrait {
    public function createComponentAsseterCss(){
        return $this->context->getService('asseter.css');

    }

    public function createComponentAsseterJs(){
        return $this->context->getService('asseter.js');

    }
    public function addJs($file){
        $this['asseterJs']->addFile($file);
        return $this;
    }
    public function addLaterJs($file){
        $this['asseterJs']->addLaterFile($file);
        return $this;
    }
    public function addCss($file){
        $this['asseterCss']->addFile($rile);
        return $this;
    }

}