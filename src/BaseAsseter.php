<?php
    /**
     * User: Jan Červený
     * Date: 16.1.2015
     * Time: 14:03
     */

    namespace Rampus\Asseter;

    /**
     * Class BaseAsseter
     *
     * @author  Jan Červeny <cerveny@redsoft.cz>
     * @package Rampus\Asseter
     */
    abstract class BaseAsseter extends \Nette\Application\UI\Control {
        /** @var  \Nette\Caching\Cache */
        public static $cache;
        protected static $temp;
        protected $files = [];
        protected $path;
        protected $filters = [];
        protected $output = [];
        protected $join = false;

        /**
         * @param                      $type
         * @param                      $path
         * @param                      $join
         * @param null                 $files
         * @param                      $filters
         * @param \Nette\Caching\Cache $cache
         * @param string               $temp
         *
         * @return \Rampus\Asseter\BaseAsseter
         */
        public static function factory($type, $path, $join, $files = null, $filters, \Nette\Caching\Cache $cache, $temp = 'temp') {
            if ($type == 'css') {
                $obj = new CSSControl();
            } else {
                $obj = new JSControl();
            }
            $obj->files = $files;
            foreach ($filters as $filter) {
                if (!is_object($filter)) {
                    $filter = new $filter;
                }
                $obj->filters[get_class($filter)] = $filter;
            }
            self::$temp = $temp;
            $obj->path = $path;
            self::$cache = $cache->derive('asseter');
            $obj->join = $join;
            return $obj;
        }

        public function addFile($file) {
            $this->files[] = $file;
            return $this;
        }

        public function addFilter($filter) {
            $this->filters[] = $filter;
            return $this;
        }

        public function render($files = null, $filters = null) {
            $this->prepareOutput($this->files);
            foreach ($this->output as $out) {
                echo $this->getElement($out) . PHP_EOL;
            }
        }

        protected function prepareOutput($files) {
            $this->output=[];
            if ($this->join) {
                $name = $this->parent->getName() . $this->parent->getAction() . $this->parent->getView();
                $name .= strpos(get_class($this), 'JSControl') ? '.js' : '.css';
                $fileCached = self::$cache->load($name);
                if (!$fileCached || !file_exists(self::$temp . DIRECTORY_SEPARATOR . $fileCached)) {
                    $content = '';
                    $fa = [];
                    foreach ($this->files as $file) {
                        $content .= file_get_contents($this->path . DIRECTORY_SEPARATOR . $file);
                        $fa[] = $this->path . DIRECTORY_SEPARATOR . $file;
                    }
                    $fh = fopen(self::$temp . DIRECTORY_SEPARATOR . $name, "w");
                    fwrite($fh, $this->applyFilters($content));
                    fclose($fh);
                    self::$cache->save($name, self::$temp . DIRECTORY_SEPARATOR . $name, [\Nette\Caching\Cache::FILES => $fa]);
                }
                $this->output[] = $this->parent->template->basePath . "/" . self::$temp . '/' . $name;
            } else {
                foreach ($files as $file) {
                    $fileCached = self::$cache->load($file);
                    if (!$fileCached || !file_exists(self::$temp . DIRECTORY_SEPARATOR . $fileCached)) {
                        $ext = pathinfo($file, PATHINFO_EXTENSION);
                        $fileCached = preg_replace("/(.css)$/", "", $file);
                        $fileCached = 'asseter-' . $fileCached . "-" . mktime(true) . '.' . $ext;
                        $content = file_get_contents($this->path . DIRECTORY_SEPARATOR . $file);
                        //copy(self::$path . DIRECTORY_SEPARATOR . $file, self::$temp . DIRECTORY_SEPARATOR . $fileCached);
                        self::$cache->save($file, $fileCached, [\Nette\Caching\Cache::FILES => $this->path . DIRECTORY_SEPARATOR . $file]);
                        $fh = fopen(self::$temp . DIRECTORY_SEPARATOR . $fileCached, "w");
                        fwrite($fh, $this->applyFilters($content));
                        fclose($fh);
                    };
                    $this->output[] = $this->parent->template->basePath . "/" . self::$temp . '/' . $fileCached;
                }
            }
        }

        protected function applyFilters($content) {
            foreach ($this->filters as $filter) {
                $content = $filter->filter($content);
            }
            return $content;
        }

    }
