<?php
    namespace Rampus\Asseter;

    /**
     * User: Jan Červený
     * Date: 16.1.2015
     * Time: 14:35
     */
    class CssMinifyFilter implements IFilter {
        /**
         * @param string $source
         *
         * @return string
         */
        public function filter($css) {
            $css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css); // remove comments
            $css = str_replace(array("\r\n", "\r", "\n", "\t", '  '), '', $css); // remove tabs, spaces, newlines, etc.
            $css = str_replace('{ ', '{', $css); // remove unnecessary spaces.
            $css = str_replace(' }', '}', $css);
            $css = str_replace('; ', ';', $css);
            $css = str_replace(', ', ',', $css);
            $css = str_replace(' {', '{', $css);
            $css = str_replace('} ', '}', $css);
            $css = str_replace(': ', ':', $css);
            $css = str_replace(' ,', ',', $css);
            $css = str_replace(' ;', ';', $css);
            return $css;
        }
    }