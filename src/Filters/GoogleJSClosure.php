<?php
    /**
     * User: Jan Červený
     * Date: 16.1.2015
     * Time: 16:46
     */

    namespace Rampus\Asseter;

    class GoogleJSClosure implements IFilter {
        /**
         * @param string $source
         *
         * @return string
         */
        public function filter($source) {
            $ch = curl_init('http://closure-compiler.appspot.com/compile');

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 'output_info=compiled_code&output_format=text&compilation_level=SIMPLE_OPTIMIZATIONS&js_code=' . urlencode($source));
            $output = curl_exec($ch);
            curl_close($ch);
            if (strlen($output) > 10) {
                return $output;
            }
            return $source;
        }
    }
