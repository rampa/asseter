<?php
/**
 * User: Jan Červený
 * Date: 16.1.2015
 * Time: 14:34
 */

namespace Rampus\Asseter;


interface IFilter {
    /**
     * @param string $source
     *
     * @return string
     */
    public function filter($source);

}