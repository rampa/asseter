<?php
    /**
     * User: Jan Červený
     * Date: 16.1.2015
     * Time: 10:04
     */

    namespace Rampus\Asseter;


    class JSControl extends BaseAsseter {
        protected $later = [];
        public function getElement($out) {
            return "<script type=\"text/javascript\" src=\"$out\"></script>" . PHP_EOL;
        }
        public function renderLater(){
            $this->prepareOutput($this->later);
            foreach ($this->output as $out) {
                echo "<script type=\"text/javascript\" src=\"$out\"></script>" . PHP_EOL;
            }

        }

        public function addLaterFile($file) {
            $this->later[]=$file;
            return $this;
        }

    }