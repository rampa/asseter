<?php
    /**
     * User: Jan Červený
     * Date: 16.1.2015
     * Time: 9:47
     */

    namespace Rampus\Asseter;

    use Nette\Configurator;
    use Nette\DI\Compiler;
    use Nette\DI\CompilerExtension;
    use Nette\DI\Config\Helpers;

    if (!class_exists('Nette\DI\CompilerExtension')) {
        class_alias('Nette\Config\CompilerExtension', 'Nette\DI\CompilerExtension');
        class_alias('Nette\Config\Compiler', 'Nette\DI\Compiler');
        class_alias('Nette\Config\Helpers', 'Nette\DI\Config\Helpers');
    }

    if (!class_exists('Nette\Configurator')) {
        class_alias('Nette\Config\Configurator', 'Nette\Configurator');
    }

    class Extension extends CompilerExtension {
        public $default = [
            'js' => [
                'type'=>'js',
                'path'=>'%wwwDir%/js',
                'filters' => [],
                'files'=>[],
                'temp'=>'asseter',
                "join"=>false
            ],
            'css' => [
                'type'=>'css',
                'path'=>'%wwwDir%/css',
                'filters' => [],
                'files'=>[],
                'temp'=>'asseter',
                "join"=>false
            ]
        ] ;

        public function loadConfiguration() {
            $config = $this->getConfig($this->default);
            $builder = $this->getContainerBuilder();
            $builder->addDefinition('asseter.js')
                    ->setFactory('\Rampus\Asseter\JSControl::factory')
                    ->setArguments($config['js']);
            $builder->addDefinition('asseter.css')
                    ->setFactory('\Rampus\Asseter\CSSControl::factory')
                    ->setArguments($config['css']);
        }
    }